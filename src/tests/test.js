// example.spec.js
const { test } = require('@playwright/test');
const { BasePage } = require('./base.js');

test.describe('Форма авторизации', () => {

  test('Ввод несуществующего номера', async ({ page }) => {
    const playwrightDev = new BasePage(page);
    await playwrightDev.goto();
    await playwrightDev.loginPopupClick();
    await playwrightDev.loginTextCheck();
    await playwrightDev.typeNumber('6990001010');
    await playwrightDev.getCodeClick();
    await playwrightDev.checkUserUndefined();
  });

  test('Ввод некорректного номера', async ({ page }) => {
    const playwrightDev = new BasePage(page);
    await playwrightDev.goto();
    await playwrightDev.loginPopupClick();
    await playwrightDev.loginTextCheck();
    await playwrightDev.typeNumber('699000110');
    await playwrightDev.getCodeClick();
    await playwrightDev.checkIncorrentNumber();
  });
})