// playwright-dev-page.js
const { MainPageSelectors } = require('./elements');

exports.BasePage = class BasePage {
    /**
     * @param {import('playwright').Page} page 
     */
    constructor(page) {
      this.page = page;
    }
  
    async goto() {
      await this.page.goto('/');
    }

    async loginPopupClick() {
      await this.page.click(MainPageSelectors.loginButton)
    }

    async loginTextCheck() {
      await this.page.waitForSelector(`text=Вход в кабинет`)
    }

    async typeNumber(phoneNumber) {
      return this.page.type(MainPageSelectors.phoneInput, phoneNumber)
    }

    async getCodeClick() {
      await this.page.click(MainPageSelectors.getCodeButton)
    }

    async checkIncorrentNumber() {
      await this.page.waitForSelector(`text=Используйте формат: +7(000) 000-00-00.`)
    }

    async checkUserUndefined() {
      await this.page.waitForSelector(`text=Пользователь не найден`)
    }

    async typeCorrectNumber() {
      return this.page.type('9000000010')
    }
  }