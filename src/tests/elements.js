const MainPageSelectors = {
    loginButton: '//div[@class="container"]//button[2]',
    phoneInput: '//form[2]/label/div/div/input',
    getCodeButton: '//form[2]/button[2]',
}

module.exports = { MainPageSelectors };