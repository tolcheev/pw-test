const config = {
    use: {
        testDir: 'tests',
        baseURL: 'https://pik-broker.ru',
        headless: true,
        browserName: 'chromium',
        screenshot: 'only-on-failure',
        timeout: 10000,
        retries: 1,
        trace: 'retain-on-failure',
        reporter: './my-awesome-reporter.js',

    },
    projects: [
        {
          name: 'Chromium',
          use: {
            browserName: 'chromium',
          },
        },
      ]
};

module.exports = config;